﻿using System;
using System.Collections.Generic;

namespace Project4SnakeLadder
{
    class Game
    {
        public enum UserChoice
        {
            Start = 0,
            Reset,
            Quit
        }

        public static void Main(string[] args)
        {
            
		}
        public class LadderSnake
        {
            public UserChoice StartMenu()
            {
                string Down = "DownArrow";
                string Enter = "Enter";
                int Col = 32, Row = 12;
                ConsoleKeyInfo MoveCursor;

                //Untuk posisi tulisan Game
                Console.SetCursorPosition(Col - 11, 9);
                Console.WriteLine("     Welcome In LadderSnake Game ");

                //untuk posisi penggeser 
                Console.BackgroundColor = ConsoleColor.DarkGreen;
                Console.SetCursorPosition(Col, 12);
                Console.WriteLine("     Start    ");
                Console.SetCursorPosition(Col, 14);
                Console.WriteLine("     Reset    ");
                Console.SetCursorPosition(Col, 16);
                Console.WriteLine("     Quit     ");

                Console.BackgroundColor = ConsoleColor.Black;
                Console.SetCursorPosition(Col -= 2, Row);
                Console.Write(">");

                while (true)
                {
                    Console.SetCursorPosition(Col, Row);
                    MoveCursor = Console.ReadKey();

                    if (MoveCursor.Key.ToString().Equals(Enter))
                    {
                        if (Row == 12)                      // START --> Row 20
                        {
                            Console.Clear();
                            StartGame();
                            return UserChoice.Start;
                        }
                        else if (Row == 14)                 // Reset --> Row 22
                        {
                            Console.Clear();
                            StartMenu();
                            return UserChoice.Reset;
                        }
                        else
                        {
                            return UserChoice.Quit;
                        }

                    }
                    else if (MoveCursor.Key.ToString().Equals(Down))
                    {
                        Console.SetCursorPosition(Col, Row += 2);
                        if (Row > 16)
                        {
                            Row = 12; // dia balik ke row 20
                            Console.SetCursorPosition(Col, Row);
                        }
                        Console.Write(">");
                    }
                    else
                    {
                        Console.SetCursorPosition(Col, Row -= 2);
                        if (Row < 12)
                        {
                            Row = 16;
                            Console.SetCursorPosition(Col, Row);
                        }
                        Console.Write(">");
                    }
                }
            }
            public void StartGame()
            {
                Console.SetCursorPosition(25, 1);
                Console.WriteLine("Snake Ladder Game");

                Console.Write("Input dimensions = ");
                int dimensions = int.Parse(Console.ReadLine());

                int areaDimensions = dimensions * dimensions;
                Map map = new Map(areaDimensions, 1);

                Random randomGenerator = new Random();

                try
                {
                    //Dekalarasi posisi yang available buat di move
                    List<MapLocation> loc = new List<MapLocation>();
                    for (int i = 0; i < areaDimensions; i++)
                    {
                        loc.Add(new MapLocation(i, 0, map));
                    }
                    Path path = new Path(loc);
                    Console.WriteLine("START from {0} and FINISH at {1}", 0, path.Length);

                    //Deklarasi player
                    List<Player> players = new List<Player>();
                    List<string> namaPlayer = new List<string>();
                    Console.Write("Masukin jumlah player = ");
                    int n = int.Parse(Console.ReadLine());
                    for (int i = 0; i < n; i++)
                        players.Add(new Player(path));

                    //masukin nama tiap player
                    for (int i = 1; i <= n; i++)
                    {
                        Console.Write("Masukkan Nama Player {0} = ", i);
                        string name = Console.ReadLine();
                        namaPlayer.Add(name);
                    }

                    //Deklarasi Snake
                    Console.Write("Total snakes = ");
                    int snake = int.Parse(Console.ReadLine());
                    Dictionary<int, int> ular = new Dictionary<int, int>();

                    //RANDOM Snake Position
                    for (int i = 0; i < snake; i++)
                    {
                        // Snake head
                        int snakeHead = randomGenerator.Next(snake + 1, areaDimensions);

                        //ngecek biar head gada yang sama
                        while (ular.ContainsKey(snakeHead))
                            snakeHead = randomGenerator.Next(snake + 1, areaDimensions);

                        // Snake tail
                        int snakeTail = randomGenerator.Next(1, snakeHead - 1);

                        Console.WriteLine("Lokasi ular ke-{0} di {1},{2}", i + 1, snakeHead, snakeTail);
                        ular.Add(snakeHead, snakeTail);
                    }

                    //Deklarasi tangga
                    Console.Write("Total ladders = ");
                    int ladder = int.Parse(Console.ReadLine());
                    Dictionary<int, int> tangga = new Dictionary<int, int>();

                    //RANDOM position ladder
                    for (int i = 0; i < ladder; i++)
                    {
                        //Ladder bottom
                        int ladderBottom = randomGenerator.Next(1, areaDimensions - ladder);
                        while (tangga.ContainsKey(ladderBottom))
                            ladderBottom = randomGenerator.Next(1, areaDimensions - ladder);

                        //Ladder top
                        int ladderTop = randomGenerator.Next(ladderBottom + 1, areaDimensions);

                        Console.WriteLine("Lokasi tangga ke-{0} di {1},{2}", i + 1, ladderBottom, ladderTop);
                        tangga.Add(ladderBottom, ladderTop);
                    }

                    //Game on
                    Level level = new Level(players, map, areaDimensions)// , ular, tangga);
                    {
                        snakes = ular,
                        ladders = tangga,
                       

                    };

                    Console.WriteLine();
                    bool playingStatus = level.isPlaying();
                    if (!playingStatus)
                    {
                        Console.WriteLine("Game Over");
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

    }
}
